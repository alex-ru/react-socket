import Image from "next/image";
import { format } from 'date-fns';

function MessageComponent(props: any) {
  if (props.isHost) {
    return (
      <div className="flex items-end justify-end mt-3 mr-2">
        <div className="flex items-start gap-2.5">
          <div className="flex flex-col justify w-full max-w-[800px] leading-1.5 p-4 border-gray-200 rounded-e-xl rounded-es-xl bg-blue-100">
            <div className="flex items-center space-x-2 rtl:space-x-reverse">
              <span className="text-sm font-semibold text-gray-900 dark:text-black">{props.user_name}</span>
              <span className="text-sm font-normal text-gray-500 dark:text-gray-400"> --- </span>
            </div>
            <p className="text-sm font-normal py-2.5 text-gray-900 dark:text-black">{props.text}</p>
            <span className="text-xs font-normal text-gray-500 dark:text-gray-400">{format(props.date, "d MMM yyyy, H:mm")}</span>
          </div>
        </div>
      </div>
    );
  }
  return (
    <div className="flex items-start mt-3">
      <div className="flex items-start gap-2.5">
        <Image className="w-8 h-8 rounded-full" src={props.user_icon} width={50} height={50} alt="Jese image" />
        <div className="flex flex-col w-full max-w-[800px] leading-1.5 p-4 border-gray-200 bg-gray-100 rounded-e-xl rounded-es-xl bg-gray-300">
          <div className="flex items-center space-x-2 rtl:space-x-reverse">
            <span className="text-sm font-semibold text-gray-900 dark:text-black">{props.user_name}</span>
            <span className="text-sm font-normal text-gray-500 dark:text-gray-400"> --- </span>
          </div>
          <p className="text-sm font-normal py-2.5 text-gray-900 dark:text-black">{props.text}</p>
          <span className="text-xs font-normal text-gray-500 dark:text-gray-400">{format(props.date, "d MMM yyyy, H:mm")}</span>
        </div>
      </div>
    </div>
  );
}

export default MessageComponent;