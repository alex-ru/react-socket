'use client'
import React from 'react';
import { ChatListComponent } from './chat-list';
import CurrentChatComponent from './current-chat';

const ChatComponent: React.FC = () => {

  return (
    <div className="flex mr-3">
      <div className="w-2/10">
        <ChatListComponent />
      </div>
      <div className="w-8/10 ml-3">
        <CurrentChatComponent />
      </div>
    </div>
  );
};

export default ChatComponent;
