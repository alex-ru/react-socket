'use client'
import React, { useEffect, useState } from 'react';
import { getSocketInstance, disconnectSocket } from '../actions/socket';
import MessageComponent from "./message";

const CurrentChatComponent: React.FC = () => {
  const [textValue, setTextValue] = useState('');
  useEffect(() => {
    const socket = getSocketInstance();

    socket.on('chat_message', (data: any) => {
      console.log('Received chat in component:', data);
    });

    return () => { disconnectSocket() };
  }, []);

  const [chatData, setChatData] = useState({
    user_1: {
      id :12,
      name: "Bonnie Green",
      last_connection: new Date(),
      icon: "https://flowbite.com/docs/images/people/profile-picture-3.jpg",
    },
    user_2: {
      id: 13,
      name: "Alex 2",
      last_connection: new Date(),
      icon: "https://flowbite.com/docs/images/people/profile-picture-3.jpg",
    },
    messages: [
      {
        user_id: 12,
        user_name: "Bonnie Green",
        user_icon: "https://flowbite.com/docs/images/people/profile-picture-3.jpg",
        date: new Date("4 jun 2020, 8:42"),
        text: "Thats awesome. I think our users will really appreciate the improvements this is de testpp p p p p p p p nnnnnnnnnnnn."
      },
      {
        user_id: 13,
        user_name: "Alex 2",
        user_icon: "https://flowbite.com/docs/images/people/profile-picture-3.jpg",
        date: new Date("4 jun 2020, 8:42"),
        text: "Thats awesome. I think our users will really appreciate the improvements qqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqq."
      }
    ]
  });

  const sendMessage = () => {
    const socket = getSocketInstance();
    socket.emit('client_message', textValue);
    console.log('Texto enviado:', textValue);
    setTextValue('');

    const userHost = chatData.user_1.id === userId ? chatData.user_1 : chatData.user_2;
    const data = {
      user_id: userHost.id,
      user_name: userHost.name,
      user_icon: userHost.icon,
      date: new Date(),
      text: textValue
    };

    setChatData(prevState => ({
      ...prevState,
      messages: [...prevState.messages, data]
    }));
  };

  const userId = 12;

  return (
    <div className="flex flex-col h-screen">
      <div className="flex-grow overflow-y-auto">
        {chatData.messages.map((item, index) => (<MessageComponent
          key={index}
          user_name={item.user_name}
          user_icon={item.user_icon}
          text={item.text}
          date={item.date}
          isHost={userId === item.user_id} />))}
      </div>

      <div className="mt-auto">
        <label htmlFor="chat" className="sr-only">Your message</label>
        <div className="flex items-center px-3 py-2 rounded-lg bg-gray-50 bg-gray-300">
          <button type="button" className="inline-flex justify-center p-2 text-gray-500 rounded-lg cursor-pointer hover:text-gray-900 hover:bg-gray-100 dark:text-gray-400 dark:hover:text-white dark:hover:bg-gray-600">
            <svg className="w-5 h-5" aria-hidden="true" xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 20 18">
              <path fill="currentColor" d="M13 5.5a.5.5 0 1 1-1 0 .5.5 0 0 1 1 0ZM7.565 7.423 4.5 14h11.518l-2.516-3.71L11 13 7.565 7.423Z" />
              <path stroke="currentColor" stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M18 1H2a1 1 0 0 0-1 1v14a1 1 0 0 0 1 1h16a1 1 0 0 0 1-1V2a1 1 0 0 0-1-1Z" />
              <path stroke="currentColor" stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M13 5.5a.5.5 0 1 1-1 0 .5.5 0 0 1 1 0ZM7.565 7.423 4.5 14h11.518l-2.516-3.71L11 13 7.565 7.423Z" />
            </svg>
            <span className="sr-only">Upload image</span>
          </button>
          <button type="button" className="p-2 text-gray-500 rounded-lg cursor-pointer hover:text-gray-900 hover:bg-gray-100 dark:text-gray-400 dark:hover:text-white dark:hover:bg-gray-600">
            <svg className="w-5 h-5" aria-hidden="true" xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 20 20">
              <path stroke="currentColor" stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M13.408 7.5h.01m-6.876 0h.01M19 10a9 9 0 1 1-18 0 9 9 0 0 1 18 0ZM4.6 11a5.5 5.5 0 0 0 10.81 0H4.6Z" />
            </svg>
            <span className="sr-only">Add emoji</span>
          </button>
          <textarea value={textValue}
            onChange={(e) => setTextValue(e.target.value)}
            rows={1} className="block mx-4 p-2.5 w-full text-sm text-gray-900 bg-white rounded-lg border border-dark-300 focus:ring-blue-500 focus:border-blue-500 bg-white" placeholder="Your message..."></textarea>
          <button onClick={sendMessage} className="inline-flex justify-center p-2 text-blue-600 rounded-full cursor-pointer hover:bg-blue-100 dark:text-blue-500 dark:hover:bg-gray-600">
            <svg className="w-5 h-5 rotate-90 rtl:-rotate-90" aria-hidden="true" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 18 20">
              <path d="m17.914 18.594-8-18a1 1 0 0 0-1.828 0l-8 18a1 1 0 0 0 1.157 1.376L8 18.281V9a1 1 0 0 1 2 0v9.281l6.758 1.689a1 1 0 0 0 1.156-1.376Z" />
            </svg>
            <span className="sr-only">Send message</span>
          </button>
        </div>
      </div>
    </div>
  );
};

export default CurrentChatComponent;
