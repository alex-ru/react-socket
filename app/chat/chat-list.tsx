import Image from "next/image"
export const ChatListComponent = () => {
  return (
    <main>
      <div className="w-full max-w-sm bg-white divide-gray-100 rounded-lg shadow dark:bg-gray-800 dark:divide-gray-700 flex flex-col h-screen">
        <div className="px-4 py-2 font-medium text-center text-gray-700 rounded-t-lg bg-gray-50 dark:bg-gray-800 dark:text-white">
          Chats
        </div>
        <div className="divide-gray-100 dark:divide-gray-700 flex-grow overflow-y-auto">
          <a href="#" className="flex px-4 py-3 hover:bg-gray-100 dark:hover:bg-gray-700">
            <div className="flex-shrink-0">
              <Image className="rounded-full w-11 h-11" src="https://flowbite.com/docs/images/people/profile-picture-4.jpg" alt="Jese image" width={50} height={50} />
            </div>
            <div className="w-full ps-3">
              <div className="text-gray-500 text-sm mb-1.5 dark:text-gray-400">New message from <span className="font-semibold text-gray-900 dark:text-white">Jese Leos</span>: "Hey, what's up? All set for the presentation?"</div>
              <div className="text-xs text-blue-600 dark:text-blue-500">1 hour ago</div>
            </div>
          </a>
          <a href="#" className="flex px-4 py-3 hover:bg-gray-100 dark:hover:bg-gray-700">
            <div className="flex-shrink-0">
              <Image className="rounded-full w-11 h-11" src="https://flowbite.com/docs/images/people/profile-picture-2.jpg" alt="Joseph image" width={50} height={50}/>
              <div className="flex items-center justify-center w-5 h-5 ms-6 -mt-5 bg-gray-900 border border-white rounded-full dark:border-gray-800">
                <svg className="w-2 h-2 text-white" aria-hidden="true" xmlns="http://www.w3.org/2000/svg" fill="currentColor" viewBox="0 0 20 18">
                  <path d="M6.5 9a4.5 4.5 0 1 0 0-9 4.5 4.5 0 0 0 0 9ZM8 10H5a5.006 5.006 0 0 0-5 5v2a1 1 0 0 0 1 1h11a1 1 0 0 0 1-1v-2a5.006 5.006 0 0 0-5-5Zm11-3h-2V5a1 1 0 0 0-2 0v2h-2a1 1 0 1 0 0 2h2v2a1 1 0 0 0 2 0V9h2a1 1 0 1 0 0-2Z" />
                </svg>
              </div>
            </div>
            <div className="w-full ps-3">
              <div className="text-gray-500 text-sm mb-1.5 dark:text-gray-400"><span className="font-semibold text-gray-900 dark:text-white">Joseph Mcfall</span> and <span className="font-medium text-gray-900 dark:text-white">5 others</span> started following you.</div>
              <div className="text-xs text-blue-600 dark:text-blue-500">10 minutes ago</div>
            </div>
          </a>
          <a href="#" className="flex px-4 py-3 hover:bg-gray-100 dark:hover:bg-gray-700">
            <div className="flex-shrink-0">
              <Image className="rounded-full w-11 h-11" src="https://flowbite.com/docs/images/people/profile-picture-3.jpg" alt="Bonnie image" width={50} height={50}/>
            </div>
            <div className="w-full ps-3">
              <div className="text-gray-500 text-sm mb-1.5 dark:text-gray-400"><span className="font-semibold text-gray-900 dark:text-white">Bonnie Green</span> and <span className="font-medium text-gray-900 dark:text-white">141 others</span> love your story. See it and view more stories.</div>
              <div className="text-xs text-blue-600 dark:text-blue-500">44 minutes ago</div>
            </div>
          </a>
          <a href="#" className="flex px-4 py-3 hover:bg-gray-100 dark:hover:bg-gray-700">
            <div className="flex-shrink-0">
              <Image className="rounded-full w-11 h-11" src="https://flowbite.com/docs/images/people/profile-picture-4.jpg" alt="Leslie image" width={50} height={50}/>
            </div>
            <div className="w-full ps-3">
              <div className="text-gray-500 text-sm mb-1.5 dark:text-gray-400"><span className="font-semibold text-gray-900 dark:text-white">Leslie Livingston</span> mentioned you in a comment: <span className="font-medium text-blue-500" href="#">@bonnie.green</span> what do you say?</div>
              <div className="text-xs text-blue-600 dark:text-blue-500">1 hour ago</div>
            </div>
          </a>
          <a href="#" className="flex px-4 py-3 hover:bg-gray-100 dark:hover:bg-gray-700">
            <div className="flex-shrink-0">
              <Image className="rounded-full w-11 h-11" src="https://flowbite.com/docs/images/people/profile-picture-4.jpg" alt="Jese image" width={50} height={50} />
            </div>
            <div className="w-full ps-3">
              <div className="text-gray-500 text-sm mb-1.5 dark:text-gray-400">New message from <span className="font-semibold text-gray-900 dark:text-white">Jese Leos</span>: "Hey, what's up? All set for the presentation?"</div>
              <div className="text-xs text-blue-600 dark:text-blue-500">1 hour ago</div>
            </div>
          </a>
          <a href="#" className="flex px-4 py-3 hover:bg-gray-100 dark:hover:bg-gray-700">
            <div className="flex-shrink-0">
              <Image className="rounded-full w-11 h-11" src="https://flowbite.com/docs/images/people/profile-picture-4.jpg" alt="Jese image" width={50} height={50} />
            </div>
            <div className="w-full ps-3">
              <div className="text-gray-500 text-sm mb-1.5 dark:text-gray-400">New message from <span className="font-semibold text-gray-900 dark:text-white">Jese Leos</span>: "Hey, what's up? All set for the presentation?"</div>
              <div className="text-xs text-blue-600 dark:text-blue-500">1 hour ago</div>
            </div>
          </a>
          <a href="#" className="flex px-4 py-3 hover:bg-gray-100 dark:hover:bg-gray-700">
            <div className="flex-shrink-0">
              <Image className="rounded-full w-11 h-11" src="https://flowbite.com/docs/images/people/profile-picture-4.jpg" alt="Jese image" width={50} height={50} />
            </div>
            <div className="w-full ps-3">
              <div className="text-gray-500 text-sm mb-1.5 dark:text-gray-400">New message from <span className="font-semibold text-gray-900 dark:text-white">Jese Leos</span>: "Hey, what's up? All set for the presentation?"</div>
              <div className="text-xs text-blue-600 dark:text-blue-500">1 hour ago</div>
            </div>
          </a>
          <a href="#" className="flex px-4 py-3 hover:bg-gray-100 dark:hover:bg-gray-700">
            <div className="flex-shrink-0">
              <Image className="rounded-full w-11 h-11" src="https://flowbite.com/docs/images/people/profile-picture-4.jpg" alt="Jese image" width={50} height={50} />
            </div>
            <div className="w-full ps-3">
              <div className="text-gray-500 text-sm mb-1.5 dark:text-gray-400">New message from <span className="font-semibold text-gray-900 dark:text-white">Jese Leos</span>: "Hey, what's up? All set for the presentation?"</div>
              <div className="text-xs text-blue-600 dark:text-blue-500">1 hour ago</div>
            </div>
          </a>
        </div>
      </div>
    </main>
  )
}