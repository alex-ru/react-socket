import Image from "next/image";

export default function Home() {
  const users = [
    {
      name: "nombre1",
      user_icon: "https://flowbite.s3.amazonaws.com/docs/gallery/square/image.jpg",
    },
    {
      name: "nombre1",
      user_icon: "https://flowbite.s3.amazonaws.com/docs/gallery/square/image-1.jpg",
    },
    {
      name: "nombre1",
      user_icon: "https://flowbite.s3.amazonaws.com/docs/gallery/square/image-2.jpg",
    },
    {
      name: "nombre1",
      user_icon: "https://flowbite.s3.amazonaws.com/docs/gallery/square/image-3.jpg",
    },
    {
      name: "nombre1",
      user_icon: "https://flowbite.s3.amazonaws.com/docs/gallery/square/image-4.jpg",
    },
    {
      name: "nombre1",
      user_icon: "https://flowbite.s3.amazonaws.com/docs/gallery/square/image-5.jpg",
    },
    {
      name: "nombre1",
      user_icon: "https://flowbite.s3.amazonaws.com/docs/gallery/square/image-6.jpg",
    },
    {
      name: "nombre1",
      user_icon: "https://flowbite.s3.amazonaws.com/docs/gallery/square/image-7.jpg",
    },
    {
      name: "nombre1",
      user_icon: "https://flowbite.s3.amazonaws.com/docs/gallery/square/image-8.jpg",
    },
    {
      name: "nombre1",
      user_icon: "https://flowbite.s3.amazonaws.com/docs/gallery/square/image-9.jpg",
    },
  ];

  return (
    <div>
      <h1 className="mb-4 text-4xl font-extrabold tracking-tight leading-none text-gray-900 md:text-5xl lg:text-6xl dark:text-black">Users active</h1>
      <div className="grid grid-cols-2 md:grid-cols-3 gap-4">
        {users.map((item, index) => (<UserItemComponent
          key={index}
          name={item.name}
          user_icon={item.user_icon}
        />))}
      </div>
    </div>
  )
}

function UserItemComponent(props: any) {
  return (
    <div>
      <Image className="h-auto max-w-full rounded-lg"
        src={props.user_icon}
        alt=""
        width={100}
        height={24} />
      <p>{props.name}</p>
    </div>
  )
}