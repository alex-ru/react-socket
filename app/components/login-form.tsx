'use client'
import { useFormState, useFormStatus } from 'react-dom'
import { loginAction, ValidateSessionAction } from '@/app/actions/auth'

export function SignInButton() {
  const { pending } = useFormStatus()
  return (
    <button aria-disabled={pending} type="submit" className="text-white bg-blue-700 hover:bg-blue-800 focus:ring-4 focus:outline-none focus:ring-blue-300 font-medium rounded-lg text-sm w-full sm:w-auto px-5 py-2.5 text-center dark:bg-blue-600 dark:hover:bg-blue-700 dark:focus:ring-blue-800">
      {pending ? 'Submitting...' : 'Sign in'}
    </button>
  );
}

export function SignInComponent() {
  ValidateSessionAction();

  const [state, loginFetch] = useFormState(loginAction, undefined);
  return (
    <main >
      <section className="bg-white dark:bg-gray-900 bg-[url('https://flowbite.s3.amazonaws.com/docs/jumbotron/hero-pattern.svg')] dark:bg-[url('https://flowbite.s3.amazonaws.com/docs/jumbotron/hero-pattern-dark.svg')]">
        <div className="py-8 px-4 mx-auto max-w-screen-xl text-center lg:py-16 z-10 relative">
          <h1 className="mb-4 text-4xl font-extrabold tracking-tight leading-none text-gray-900 md:text-5xl lg:text-6xl dark:text-white">Login and start to chat</h1>
          <form className="w-full max-w-md mx-auto" action={loginFetch} >
            <div className="mb-1">
              <label htmlFor="email" className="block mb-2 text-sm font-medium text-gray-900 dark:text-white">username</label>
              <input type="text" id="email" name="email" className="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500" placeholder="username" required />
            </div>

            {state?.errors?.email && <p>{state.errors.email}</p>}

            <div className="mb-1">
              <label htmlFor="password" className="block mb-2 text-sm font-medium text-gray-900 dark:text-white">Your password</label>
              <input type="password" id="password" name="password" className="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500" required />
            </div>

            <div className="flex items-start mb-1">
              {state?.errors?.password && (
                <div>
                  <p>Password must:</p>
                  <ul>
                    {state.errors.password.map((error) => (<li key={error}>- {error}</li>))}
                  </ul>
                </div>
              )}
            </div>
            <SignInButton />
          </form>
        </div>
        <div className="bg-gradient-to-b from-blue-50 to-transparent dark:from-blue-900 w-full h-full absolute top-0 left-0 z-0"></div>
      </section>
    </main>
  );
}


