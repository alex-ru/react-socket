import 'server-only'
import { cookies } from 'next/headers'
import { jwtDecode } from "jwt-decode";

export async function createSession(token: string) {
  const decodedToken = jwtDecode(token);

  cookies().set('session', token, {
    httpOnly: true,
    secure: true,
    expires: new Date(decodedToken.exp! * 1000),
    sameSite: 'lax',
    path: '/',
  })
}

export function getSession(){
  const token = cookies().get('session')?.value;
  if(!token) return;
  const user = jwtDecode(token!);

  return {token, user};
}

export function deleteSession() {
  cookies().delete('session')
}