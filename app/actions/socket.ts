import io from 'socket.io-client';

const urlApi = "http://127.0.0.1:5000";
let socketInstance: SocketIOClient.Socket | null = null;

const getSocketInstance = (): SocketIOClient.Socket => {
  if (!socketInstance) {
    socketInstance = io(urlApi);
    //setupSocketHandlers(socketInstance);
  }
  return socketInstance;
};

/*const setupSocketHandlers = (socket: SocketIOClient.Socket) => {
  socket.on('connect', () => {
    console.log('Connected to server');
  });

  socket.on('disconnect', () => {
    console.log('Disconnected from server');
  });

  socket.on('chat_response', (data: any) => {
    console.log('Received chat response:', data);
  });

  socket.on('notification_response', (data: any) => {
    console.log('Received notification response:', data);
  });
};*/

const disconnectSocket = () => {
  if (socketInstance) {
    socketInstance.disconnect();
    socketInstance = null;
  }
};

export { getSocketInstance, disconnectSocket };
