'use server'
import { SignupFormSchema, FormState } from '@/app/lib/definitions'
import axios from "axios";
import { createSession, getSession } from '../lib/session';
import { redirect } from 'next/navigation'


export async function loginAction(state: FormState, formData: any) {
  const url = process.env.API_BASE_URL + "/auth/login";

  const username = formData.get('email');
  const password = formData.get('password');
  let redirectPath = "/login";

  try {
    const response = await axios.post(url!, {
      username,
      password,
    }, {
      headers: { "Content-Type": "application/json" }
    });

    const token = response.data.token;
    await createSession(token);

    redirectPath = "/people";
  } catch (error: any) {
    const _error: any = error?.response?.data || "API error";
    console.log({ error });
  } finally {
    redirect(redirectPath);
    return true;
  }
}

export async function ValidateSessionAction() {
  const session = getSession();
  if(!session?.user?.exp) {
    return;
  }

  const expirationDate = new Date(session.user.exp * 1000);
  const currentDate = new Date();
  const isExpired = expirationDate < currentDate;

  if(!isExpired){
    redirect('/people');
  }
}

