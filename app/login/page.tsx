import React from "react";
import { SignInComponent  } from "../components/login-form";

const loginLayoutStyles = {
  //minHeight: "100vh",
  display: "grid",
  //gridTemplateColumns: "48rem",
  alignContent: "center",
  justifyContent: "center",
};

function Page() {
  return (
    <main style={loginLayoutStyles}>
      <SignInComponent />
    </main>
  );
}

export default Page;
