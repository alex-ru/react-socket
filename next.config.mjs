/** @type {import('next').NextConfig} */
const nextConfig = {
    images: {
        domains: ['flowbite.s3.amazonaws.com', 'flowbite.com'], // Agrega el host de la imagen aquí
    },
    env: {
      API_BASE_URL: process.env.API_BASE_URL,
    },
};

export default nextConfig;
